% RAP(1) Manual de RAP | rap
% fauno <fauno@endefensadelsl.org>
% 2013

# NOMBRE

RAP

## SINOPSIS

  rap comando -opciones parametros


# OPCIONES

-h
:    Ayuda

-c
:    Lista de comandos disponibles

-d
:    Habilitar debug.  Para hacer debug de los comandos, agregar esta
     opción.


# DESCRIPCION

## ¿Cómo empiezo?

Leyendo la ayuda de _rap init_ :)

## ¿Donde está mi nodo?

El comando _rap init_ crea tu nodo dentro del directorio _nodos/_.

Podés tener varios nodos pero instalar uno por sistema (usando _rap
install tu-nodo_).

Cualquier comando que aplique cambios en tu nodo, debe instalarse luego
usando el comando _rap install tu-nodo_.

Consultar la ayuda de cada comando usando la opción -h luego del nombre
de comando:

	rap add-host -h


# VER TAMBIEN

_rap-init(1)_
