% RAP-CONNECTTO(1) Manual de RAP | rap
% fauno <fauno@endefensadelsl.org>
% 2013

# NOMBRE

Se conecta con otros nodos


# SINOPSIS

_rap connectto_ nodo-local [nodo-remoto nodo-remoto2...]


# DESCRIPCION

Configura el nodo local para conectarse a los nodos remotos
especificados.  Para poder ingresar a una red autónoma pirata, hay que
copiar el archivo de host del nodo remoto en el directorio hosts/
y agregar la línea _ConnectTo = nodo-remoto_ en _tinc.conf_.  Este
script automatiza este paso.

Los nodos remotos deben ejecutar el comando _rap add-host_ con el
nombre del nodo local.


# EJEMPLOS

## Listar los nodos a los que noanoa se conecta

    rap connectto noanoa

## Agregar los nodos ponape y medieval al nodo noanoa

    rap connectto noanoa ponape medieval


# VER TAMBIEN

_tinc.conf(5)_, _rap-add-host(1)_
